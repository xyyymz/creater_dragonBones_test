// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.scheduleOnce(function(){
            this.myStart()
        }, 1)
    },

    myStart () {

        this._armatureDisPlay = this.getComponent(dragonBones.ArmatureDisplay)
        this._armatureDisPlay.armatureName = 'robot'
        //获取 Armatrue
        this._armature = this._armatureDisPlay.armature()
        //获取 animation
        this._animations = this._armature.animation
        var animations = this._animations._animationNames
        // cc.log(animations)
        // this._armatureDisPlay.playAnimation(animations[1], 1)
        this._armature.animation.fadeIn(animations[2], 15, -1, 0, 'hit');
        //https://developer.egret.com/cn/apidoc/index/name/dragonBones.Animation
        //https://docs.cocos.com/creator/api/zh/classes/ArmatureDisplay.html
    },

    // update (dt) {

    // },
    
    // onDestroy () {
    // },
});
