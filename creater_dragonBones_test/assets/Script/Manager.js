// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        ifDev: false,
        layerNum: {
            type: cc.Integer, //使用整型定义
            default: 0
        },
        layerArr: {
            type: cc.Prefab, 
            default: []
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    start () {
        // this.children = this.layerArr;
        if(this.ifDev){
            this.index = this.layerNum
        }else{
            this.index = 0
        }

        if(this.layerArr[this.index]){
            this.LayerPrefab = cc.instantiate(this.layerArr[this.index]);
            this.node.addChild(this.LayerPrefab);
        }
    },

    onKeyUp (event) {
        switch(event.keyCode) {
            case cc.macro.KEY.left:
                if(!cc.game.isPaused()){
                    this.prePage()
                }
                break;
            case cc.macro.KEY.right:
                if(!cc.game.isPaused()){
                    this.nextPage()
                }
                break;
            case cc.macro.KEY.shift:
                if(!cc.game.isPaused()){
                    this.boxDisplay = !this.boxDisplay
                }
                break;
            case cc.macro.KEY.space:
                if(cc.game.isPaused()){
                    cc.game.resume()
                }else{
                    cc.game.pause()
                }
                break;
            default:
                break;
        }
    },
    
    nextPage () {
        this.unscheduleAllCallbacks()
        cc.audioEngine.pauseAll();
        if(this.index < this.layerArr.length - 1){
            var Flag_child = this.LayerPrefab.getComponent('Flag_child')
            if(Flag_child && Flag_child.flagLen && ((Flag_child.flagIndex + 1) !== Flag_child.flagLen)){
                this.LayerPrefab.opacity = 0
                var LayerPrefab = this.LayerPrefab = cc.instantiate(this.layerArr[this.index + 1])
                this.node.addChild(LayerPrefab);
            }else{
                this.LayerPrefab.runAction(cc.fadeOut(1))
                var LayerPrefab = this.LayerPrefab = cc.instantiate(this.layerArr[this.index + 1])
                LayerPrefab.opacity = 1
                this.node.addChild(LayerPrefab);
                LayerPrefab.runAction(cc.fadeIn(1))
            }
            this.index++
        }else{
            cc.log('finish')
        }
    },

    prePage () {
        this.unscheduleAllCallbacks()
        cc.audioEngine.pauseAll();
        if(this.index > 0){
            var Flag_child = this.LayerPrefab.getComponent('Flag_child')
            if(Flag_child && Flag_child.flagLen && (Flag_child.flagIndex!== 0)){
                this.LayerPrefab.opacity = 0
                var LayerPrefab = this.LayerPrefab = cc.instantiate(this.layerArr[this.index - 1])
                this.node.addChild(LayerPrefab);
            }else{
                this.LayerPrefab.runAction(cc.fadeOut(1))
                var LayerPrefab = this.LayerPrefab = cc.instantiate(this.layerArr[this.index - 1])
                LayerPrefab.opacity = 1
                this.node.addChild(LayerPrefab);
                LayerPrefab.runAction(cc.fadeIn(1))
            }
            this.index--
        }else{
            cc.log('已经第一页了')
        }
    },
    
    // update (dt) {},

    onDestroy () {
        // cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },
});

