"use strict";
cc._RF.push(module, '382ec9xIkpDOI/NAnKKk+fT', 'SetAnchor');
// Script/SetAnchor.js

"use strict";

cc.Class({
    extends: cc.Component,
    //编辑器属性定义
    properties: {
        myAnchorX: {
            type: cc.Float,
            default: 0
            //使用notify函数监听属性变化
            // notify(oldValue) {
            //     //减少无效赋值
            //     if (oldValue === this.zIndex) {
            //         return;
            //     }
            //     this.node.anchorX = this.myAnchorX;
            //     // this.node.x += this.node.width * this.myAnchorX
            // }
        },
        myAnchorY: {
            type: cc.Float,
            default: 0
            //使用notify函数监听属性变化
            // notify(oldValue) {
            //     //减少无效赋值
            //     if (oldValue === this.zIndex) {
            //         return;
            //     }
            //     this.node.anchorY = this.myAnchorY;
            //     // this.node.y += this.node.width * this.myAnchorY
            // }
        }
    },
    onLoad: function onLoad() {
        var oldAnchorX = this.node.anchorX;
        var oldAnchorY = this.node.anchorY;
        this.node.anchorX = this.myAnchorX;
        this.node.anchorY = this.myAnchorY;
        if (this.node.scaleX !== 0) {
            this.node.x += this.node.width * (this.myAnchorX - oldAnchorX) * this.node.scaleX;
        } else {
            this.node.x += this.node.width * (this.myAnchorX - oldAnchorX);
        }
        this.node.y += this.node.height * (this.myAnchorY - oldAnchorY) * this.node.scaleY;
    }
});

cc._RF.pop();