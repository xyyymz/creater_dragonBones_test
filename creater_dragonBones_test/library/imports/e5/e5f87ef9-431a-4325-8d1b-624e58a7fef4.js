"use strict";
cc._RF.push(module, 'e5f8775QxpDJY0bYk5Yp/70', 'ChildPos');
// Script/ChildPos.js

"use strict";

cc.Class({
    extends: cc.Component,
    //编辑器属性定义
    properties: {
        myPositionX: {
            type: cc.Integer,
            default: 0,
            //使用notify函数监听属性变化
            notify: function notify(oldValue) {
                //减少无效赋值
                if (oldValue === this.myPositionX) {
                    return;
                }
                var parent = this.node.parent;
                this.node.x = this.myPositionX - parent.x;
            }
        },
        myPositionY: {
            type: cc.Integer,
            default: 0,
            //使用notify函数监听属性变化
            notify: function notify(oldValue) {
                //减少无效赋值
                if (oldValue === this.myPositionY) {
                    return;
                }
                var parent = this.node.parent;
                this.node.y = this.myPositionY - parent.y;
            }
        }
    },
    onLoad: function onLoad() {
        var parent = this.node.parent;
        this.node.x = this.myPositionX - parent.x;
        this.node.y = this.myPositionY - parent.y;
    }
});

cc._RF.pop();