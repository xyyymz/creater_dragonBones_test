"use strict";
cc._RF.push(module, '04380TVL2xHrbLkB8UBHEM6', 'SetZIndex');
// Script/SetZIndex.js

"use strict";

/**
*SetZIndex.js 控制组件
*https://www.jianshu.com/p/f92e48467ce8
**/
cc.Class({
    extends: cc.Component,
    //编辑器属性定义
    properties: {
        zIndex: {
            type: cc.Integer, //使用整型定义
            default: 0,
            //使用notify函数监听属性变化
            notify: function notify(oldValue) {
                //减少无效赋值
                if (oldValue === this.zIndex) {
                    return;
                }
                this.node.zIndex = this.zIndex;
            }
        }
    },
    onLoad: function onLoad() {
        this.node.zIndex = this.zIndex;
    }
});

cc._RF.pop();