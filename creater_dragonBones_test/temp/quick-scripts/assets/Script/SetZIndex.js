(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/Script/SetZIndex.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '04380TVL2xHrbLkB8UBHEM6', 'SetZIndex', __filename);
// Script/SetZIndex.js

"use strict";

/**
*SetZIndex.js 控制组件
*https://www.jianshu.com/p/f92e48467ce8
**/
cc.Class({
    extends: cc.Component,
    //编辑器属性定义
    properties: {
        zIndex: {
            type: cc.Integer, //使用整型定义
            default: 0,
            //使用notify函数监听属性变化
            notify: function notify(oldValue) {
                //减少无效赋值
                if (oldValue === this.zIndex) {
                    return;
                }
                this.node.zIndex = this.zIndex;
            }
        }
    },
    onLoad: function onLoad() {
        this.node.zIndex = this.zIndex;
    }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=SetZIndex.js.map
        